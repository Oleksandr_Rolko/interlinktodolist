-- auto-generated definition
create table task_in_list
(
  task_id integer not null
    constraint task_list_task_id_fk
      references task
      on update cascade on delete cascade,
  list_id integer not null
    constraint task_in_list_list_id_fk
      references list
      on update cascade on delete cascade
);

alter table task_in_list
  owner to postgres;
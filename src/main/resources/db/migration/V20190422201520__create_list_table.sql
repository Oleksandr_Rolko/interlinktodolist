-- auto-generated definition
create table list
(
  id   serial       not null
    constraint list_pk
      primary key,
  name varchar(100) not null
);

alter table list
  owner to postgres;
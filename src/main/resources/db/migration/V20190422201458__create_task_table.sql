-- auto-generated definition
create table task
(
  id        serial                not null
    constraint task_pk
      primary key,
  text      varchar(200)          not null,
  date_time timestamp             not null,
  is_done   boolean default false not null
);

alter table task
  owner to postgres;
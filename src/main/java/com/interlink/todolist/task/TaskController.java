package com.interlink.todolist.task;

import com.interlink.todolist.tasklist.TaskList;
import com.interlink.todolist.tasklist.TaskListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

@Controller
public class TaskController {

    private final TaskService taskService;
    private final TaskListService taskListService;

    @Autowired
    public TaskController(TaskService taskService, TaskListService taskListService) {
        this.taskService = taskService;
        this.taskListService = taskListService;
    }

    @GetMapping("/task-manager")
    public String getTaskManagerForm(Model model) {
        model.addAttribute("taskDto", new TaskDto());
        model.addAttribute("taskList", new TaskList());
        return "task_manager";
    }

    @GetMapping("/get/tasks/{listId}")
    @ResponseBody
    public List<TaskDto> getTasks(@PathVariable int listId) {
        return taskService.getTasksByListId(listId);
    }

    @PostMapping("/task-manager")
    public String addTask(@Valid TaskDto taskDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("taskDto", taskDto);
            return "task_manager";
        }

        taskService.addTask(taskDto);

        return "redirect:/task-manager";
    }

    @PostMapping("/task/{id}/{isDone}")
    @ResponseBody
    public void changeDone(@PathVariable int id, @PathVariable boolean isDone) {
        taskService.changeDone(id, isDone);
    }

    @PostMapping("/task/{id}/delete")
    @ResponseBody
    public void deleteTask(@PathVariable int id) {
        taskService.deleteTask(id);
    }
}

package com.interlink.todolist.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class TaskRepository {

    private static final String INSERT_TASK = "INSERT INTO task (text, date_time) VALUES (:text, :date_time)";
    private static final String INSERT_TASK_IN_LIST =
            "INSERT INTO task_in_list (task_id, list_id) VALUES (:task_id, :list_id)";
    private static final String SELECT_TASKS_BY_LIST_ID = "" +
            "SELECT * FROM task t " +
            "LEFT JOIN task_in_list til on t.id = til.task_id " +
            "WHERE list_id = :list_id " +
            "ORDER BY date_time";
    private static final String DELETE_TASK = "DELETE FROM task WHERE id = :id";
    private static final String UPDATE_DONE_TASK = "UPDATE task SET is_done = :is_done WHERE id = :id";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final TaskRowMapper taskRowMapper;

    @Autowired
    public TaskRepository(DataSource dataSource, TaskRowMapper taskRowMapper) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.taskRowMapper = taskRowMapper;
    }


    public void addTask(Task task) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("text", task.getText())
                .addValue("date_time", task.getDateTime());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(INSERT_TASK, parameters, keyHolder, new String[] { "id" });

        parameters.addValue("task_id", Objects.requireNonNull(keyHolder.getKey()).intValue());
        parameters.addValue("list_id", task.getTaskList().getId());
        jdbcTemplate.update(INSERT_TASK_IN_LIST, parameters);
    }

    public List<Task> getTasksByListId(int listId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("list_id", listId);
        try {
            return jdbcTemplate.query(SELECT_TASKS_BY_LIST_ID, parameters, taskRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return Collections.emptyList();
        }
    }

    public void changeDone(int id, boolean isDone) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("is_done", isDone);
        jdbcTemplate.update(UPDATE_DONE_TASK, parameters);
    }

    public void deleteTask(int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("id", id);
        jdbcTemplate.update(DELETE_TASK, parameters);
    }
}

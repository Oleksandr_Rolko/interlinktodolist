package com.interlink.todolist.task;

import com.interlink.todolist.tasklist.TaskListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TaskRowMapper implements RowMapper<Task> {

    private final TaskListRepository taskListRepository;

    @Autowired
    public TaskRowMapper(TaskListRepository taskListRepository) {
        this.taskListRepository = taskListRepository;
    }

    @Override
    public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
        Task task = new Task();

        task.setId(rs.getInt("id"));
        task.setText(rs.getString("text"));
        task.setDateTime(rs.getTimestamp("date_time").toLocalDateTime());
        task.setDone(rs.getBoolean("is_done"));
        task.setTaskList(taskListRepository.getTaskListById(rs.getInt("list_id")));

        return task;
    }
}

package com.interlink.todolist.task;

import com.interlink.todolist.tasklist.TaskListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskListRepository taskListRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository, TaskListRepository taskListRepository) {
        this.taskRepository = taskRepository;
        this.taskListRepository = taskListRepository;
    }

    public void addTask(TaskDto taskDto) {
        taskRepository.addTask(createTask(taskDto));
    }

    public List<TaskDto> getTasksByListId(int listId) {
        return taskRepository.getTasksByListId(listId)
                .stream()
                .map(this::createTaskDto)
                .collect(Collectors.toList());
    }

    public void changeDone(int id, boolean isDone) {
        taskRepository.changeDone(id, isDone);
    }

    public void deleteTask(int id) {
        taskRepository.deleteTask(id);
    }

    private TaskDto createTaskDto(Task task) {
        TaskDto taskDto = new TaskDto();

        taskDto.setId(task.getId());
        taskDto.setText(task.getText());
        taskDto.setDate(task.getDateTime().toLocalDate());
        taskDto.setTime(task.getDateTime().toLocalTime());
        taskDto.setDone(task.isDone());
        taskDto.setTaskListId(task.getTaskList().getId());

        return taskDto;

    }

    private Task createTask(TaskDto taskDto) {
        Task task = new Task();

        task.setId(taskDto.getId());
        task.setText(taskDto.getText());
        task.setDateTime(LocalDateTime.of(taskDto.getDate(), taskDto.getTime()));
        task.setDone(taskDto.isDone());
        task.setTaskList(taskListRepository.getTaskListById(taskDto.getTaskListId()));

        return task;
    }
}

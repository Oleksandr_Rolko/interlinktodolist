package com.interlink.todolist.tasklist;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class TaskList {

    private int id;

    @NotNull
    @NotEmpty
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

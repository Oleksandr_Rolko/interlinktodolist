package com.interlink.todolist.tasklist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class TaskListRepository {

    private static final String INSERT_LIST = "INSERT INTO list (name) VALUES (:name)";
    private static final String SELECT_ALL_LISTS = "SELECT * FROM list ORDER BY name ";
    private static final String SELECT_LIST_BY_ID = "SELECT * FROM list WHERE id = :id";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final TaskListRowMapper taskListRowMapper;

    @Autowired
    public TaskListRepository(DataSource dataSource, TaskListRowMapper taskListRowMapper) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.taskListRowMapper = taskListRowMapper;
    }

    public void addList(TaskList taskList) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("name", taskList.getName());
        jdbcTemplate.update(INSERT_LIST, parameters);
    }

    public List<TaskList> getAllTaskLists() {
        return jdbcTemplate.query(SELECT_ALL_LISTS, taskListRowMapper);
    }

    public TaskList getTaskListById(int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.queryForObject(SELECT_LIST_BY_ID, parameters, taskListRowMapper);
    }
}

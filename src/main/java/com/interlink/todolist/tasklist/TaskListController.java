package com.interlink.todolist.tasklist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

@Controller
public class TaskListController {

    private final TaskListService taskListService;

    @Autowired
    public TaskListController(TaskListService taskListService) {
        this.taskListService = taskListService;
    }

    @GetMapping("/get/lists")
    @ResponseBody
    public List<TaskList> getLists() {
        return taskListService.getAllTaskLists();
    }

    @PostMapping("/add/list")
    public String addTaskList(@Valid TaskList taskList, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(taskList);
            return "redirect:/task-manager";
        }

        taskListService.addList(taskList);

        return "redirect:/task-manager";
    }
}

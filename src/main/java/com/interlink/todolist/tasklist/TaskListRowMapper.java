package com.interlink.todolist.tasklist;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TaskListRowMapper implements RowMapper<TaskList> {

    @Override
    public TaskList mapRow(ResultSet rs, int rowNum) throws SQLException {
        TaskList taskList = new TaskList();

        taskList.setId(rs.getInt("id"));
        taskList.setName(rs.getString("name"));

        return taskList;
    }
}
